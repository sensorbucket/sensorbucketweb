import styled from 'styled-components';

/* eslint-disable-next-line */
export interface UnitTypesProps {}

export function UnitTypes(props: UnitTypesProps) {
  return (
    <StyledContainer>
      <h1>Meet eenheden</h1>
    </StyledContainer>
  );
}

export default UnitTypes;

const StyledContainer = styled.div`
  padding: 1rem;
  display: grid;
  grid-template-rows: auto auto 1fr auto;
  height: 100%;
  width: 100%;
  box-sizing: border-box;
`;