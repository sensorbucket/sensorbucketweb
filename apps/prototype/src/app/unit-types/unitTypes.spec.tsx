import { render } from '@testing-library/react';

import UnitTypes from './unitTypes';

describe('UnitTypes', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UnitTypes />);
    expect(baseElement).toBeTruthy();
  });
});
