import { render } from '@testing-library/react';

import Measurements from './measurements';

describe('Measurements', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Measurements />);
    expect(baseElement).toBeTruthy();
  });
});
