import styled from 'styled-components';

/* eslint-disable-next-line */
export interface UsersProps {}

export function Users(props: UsersProps) {
  return (
    <StyledContainer>
      <h1>Gebruikers</h1>
    </StyledContainer>
  );
}

export default Users;

const StyledContainer = styled.div`
  padding: 1rem;
  display: grid;
  grid-template-rows: auto auto 1fr auto;
  height: 100%;
  width: 100%;
  box-sizing: border-box;
`;