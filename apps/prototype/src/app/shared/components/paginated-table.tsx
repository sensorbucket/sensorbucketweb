/* eslint-disable @typescript-eslint/no-explicit-any */
import styled from 'styled-components';
import { useTable, usePagination, Row } from 'react-table';
import React, { useState } from 'react';
import Select from 'react-select';
import { DateRangePicker } from 'react-date-range';
import { ReactComponent as Calendar } from '../../../assets/svg/calendar.svg';

/* eslint-disable-next-line */
export interface PaginatedTableProps {}

export function PaginatedTable(props: PaginatedTableProps) {
  const range = (len: number) => {
    const arr = [];
    for (let i = 0; i < len; i++) {
      arr.push(i);
    }
    return arr;
  };

  const newMeasurement = () => {
    const organisations = ['Organisatie 1', 'Organisatie 2', 'Organisatie 3'];
    const locations = [
      'Den Haag',
      'Breda',
      'Rotterdam',
      'Amsterdam',
      'Eindhoven',
      'Zeeland',
    ];
    const units = ['°C', 'Bar', '%'];
    const timestamp = new Date(
      2022,
      3,
      Math.ceil(Math.random() * 14),
      Math.floor(Math.random() * 24),
      Math.floor(Math.random() * 60)
    );
    return {
      measurementID: Math.floor(Math.random() * 10000).toString(),
      measurementSensorID: Math.floor(Math.random() * 100).toString(),
      measurementSensorDescription: Math.floor(Math.random() * 100).toString(),
      measurementTimestamp: timestamp,
      measurementTimestampLabel: timestamp.toISOString(),
      measurementCoordinates: Math.floor(Math.random() * 100).toString(),
      measurementValue: Math.floor(Math.random() * 100).toString(),
      measurementDeviceID: Math.floor(Math.random() * 100).toString(),
      measurementDeviceDescription: Math.floor(Math.random() * 100).toString(),
      measurementDeviceType: Math.floor(Math.random() * 100).toString(),
      measurementDeviceTypeDescription: Math.floor(
        Math.random() * 100
      ).toString(),
      measurementDeviceTypeMobile: Math.floor(Math.random() * 100).toString(),
      measurementTypeID: Math.floor(Math.random() * 100).toString(),
      measurementTypeName: Math.floor(Math.random() * 100).toString(),
      measurementTypeDescription: Math.floor(Math.random() * 100).toString(),
      measurementTypeUnit: units[Math.floor(Math.random() * units.length)],
      measurementOrganisationID: Math.floor(Math.random() * 100).toString(),
      measurementOrganisationName:
        organisations[Math.floor(Math.random() * organisations.length)],
      measurementOrganisationAddress: Math.floor(
        Math.random() * 100
      ).toString(),
      measurementOrganisationZipcode: Math.floor(
        Math.random() * 100
      ).toString(),
      measurementOrganisationCity: Math.floor(Math.random() * 100).toString(),
      measurementLocationID: Math.floor(Math.random() * 100).toString(),
      measurementLocationName:
        locations[Math.floor(Math.random() * locations.length)],
      measurementLocationAddress: Math.floor(Math.random() * 100).toString(),
      measurementLocationCity: Math.floor(Math.random() * 100).toString(),
    };
  };

  function makeData(...lens: number[]) {
    const makeDataLevel: any = (depth = 0) => {
      const len = lens[depth];
      return range(len).map((d) => {
        return {
          ...newMeasurement(),
          subRows: lens[depth + 1] ? makeDataLevel(depth + 1) : undefined,
        };
      });
    };

    return makeDataLevel();
  }

  const columns = React.useMemo(
    () => [
      {
        Header: 'Metingen',
        columns: [
          {
            Header: 'Tijdstip',
            accessor: 'measurementTimestampLabel',
          },
          {
            Header: 'Waarde',
            accessor: 'measurementValue',
          },
          {
            Header: 'Meet eenheid',
            accessor: 'measurementTypeUnit',
          },
          {
            Header: 'Organisatie',
            accessor: 'measurementOrganisationName',
          },
          {
            Header: 'Locatie',
            accessor: 'measurementLocationName',
          },
        ],
      },
    ],
    []
  );
  // eslint-disable-next-line react-hooks/exhaustive-deps
  let data = React.useMemo(() => makeData(1000), []).sort(
    (a: any, b: any) => b.measurementTimestamp - a.measurementTimestamp
  );
  data = data.sort((e: any) => e.measurementValue);
  return (
    <StyledContainer>
      <Filters></Filters>
      <Table columns={columns} data={data} />
    </StyledContainer>
  );
}

function Filters() {
  const devices = [
    { value: 'Apparaat 1', label: 'Apparaat 1' },
    { value: 'Apparaat 2', label: 'Apparaat 2' },
    { value: 'Apparaat 3', label: 'Apparaat 3' },
  ];
  const sensors = [
    { value: 'Sensor 1', label: 'Sensor 1' },
    { value: 'Sensor 2', label: 'Sensor 2' },
    { value: 'Sensor 3', label: 'Sensor 3' },
  ];
  const dateRange = {
    startDate: new Date(),
    endDate: new Date(),
    key: 'selection',
  };

  const getIcon = () => {
    const Icon = Calendar;
    return <Icon />;
  };
  const [selectedDevice, setSelectedDevice] = useState(devices[0]);
  const [selectedSensor, setSelectedSensor] = useState(sensors[0]);
  const [selectedDateRange, setSelectedDateRange] = useState(dateRange);
  const [dateRangePickerOpen, setDateRangePickerOpen] = useState(false);

  return (
    <StyledRow>
      <DateRangePickerContainer $active={dateRangePickerOpen}>
        <DateRangePicker
          ranges={[selectedDateRange]}
          onChange={(ranges) =>
            setSelectedDateRange({
              startDate: ranges['selection'].startDate ?? new Date(),
              endDate: ranges['selection'].endDate ?? new Date(),
              key: 'selection',
            })
          }
        ></DateRangePicker>
      </DateRangePickerContainer>
      <InputContainer>
        <label>Tijdstip van</label>
        <label>{selectedDateRange.startDate.toDateString()}</label>
      </InputContainer>
      <IconWrapper
        $active={dateRangePickerOpen}
        onClick={() => setDateRangePickerOpen(!dateRangePickerOpen)}
      >
        {getIcon()}
      </IconWrapper>
      <InputContainer>
        <label>Tijdstip tot</label>
        <label>{selectedDateRange.endDate.toDateString()}</label>
      </InputContainer>
      <InputContainer>
        <label>Apparaat</label>
        <Select
          options={devices}
          value={selectedDevice}
          onChange={(value: any) => setSelectedDevice(value)}
        ></Select>
      </InputContainer>
      <InputContainer>
        <label>Sensor</label>
        <Select
          options={sensors}
          value={selectedSensor}
          onChange={(value: any) => setSelectedSensor(value)}
        ></Select>
      </InputContainer>
    </StyledRow>
  );
}

function Table({ columns, data }: { columns: any; data: any }) {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 },
    },
    usePagination
  );

  return (
    <>
      <Scrollable>
        <table {...getTableProps()}>
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th {...column.getHeaderProps()}>
                    {column.render('Header')}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row: Row<object>, i: any) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </Scrollable>
      <div className="pagination">
        <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
          {'<<'}
        </button>{' '}
        <button onClick={() => previousPage()} disabled={!canPreviousPage}>
          {'<'}
        </button>{' '}
        <button onClick={() => nextPage()} disabled={!canNextPage}>
          {'>'}
        </button>{' '}
        <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
          {'>>'}
        </button>{' '}
        <span>
          Page{' '}
          <strong>
            {pageIndex + 1} of {pageOptions.length}
          </strong>{' '}
        </span>
        <span>
          | Go to page:{' '}
          <input
            type="number"
            defaultValue={pageIndex + 1}
            onChange={(e) => {
              const page = e.target.value ? Number(e.target.value) - 1 : 0;
              gotoPage(page);
            }}
            style={{ width: '100px' }}
          />
        </span>{' '}
        <select
          value={pageSize}
          onChange={(e) => {
            setPageSize(Number(e.target.value));
          }}
        >
          {[10, 20, 30, 40, 50].map((pageSize) => (
            <option key={pageSize} value={pageSize}>
              Show {pageSize}
            </option>
          ))}
        </select>
      </div>
    </>
  );
}

export default PaginatedTable;

const StyledContainer = styled.div`
  padding-left: 2rem;

  table {
    border-spacing: 0;
    border: 1px solid black;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.1rem 0.5em;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
  }

  .pagination {
    padding: 0.5rem 0.1rem;
  }
`;

const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 150px;
  margin-bottom: 20px;
  margin-right: 10px;
`;

const DateRangePickerContainer = styled.div<{ $active: boolean }>`
  display: ${(props) => (props.$active ? 'flex' : 'none')};
  z-index: 999;
  position: ${(props) => (props.$active ? 'absolute' : 'relative')};
  margin-top: 60px;
  ${(props) => `background-color:${props.theme.primaryDefault};`}
  padding: 5px;
`;

const Scrollable = styled.div`
  overflow: scroll;
  max-height: 55vh;
`;

const StyledRow = styled.div`
  display: flex;
  flex-direction: row;
`;

const IconWrapper = styled.div<{ $active: boolean }>`
  height: 50px;
  width: 30px;
  padding: 5px;
  margin-right: 10px;
  border-radius: 3px;
  ${(props) =>
    props.$active && `background-color:${props.theme.primaryDefault};`}
  display: flex;
  align-items: center;
  box-sizing: border-box;

  svg {
    fill: ${(props) => (props.$active ? '#fff;' : `${props.theme.fontColor}`)};
    height: auto;
    width: 1.1rem;
  }
`;
