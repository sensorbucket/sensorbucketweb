import styled from 'styled-components';

/* eslint-disable-next-line */
export interface LocationsProps {}

export function Locations(props: LocationsProps) {
  return (
    <StyledContainer>
      <h1>Locaties</h1>
    </StyledContainer>
  );
}

export default Locations;

const StyledContainer = styled.div`
  padding: 1rem;
  display: grid;
  grid-template-rows: auto auto 1fr auto;
  height: 100%;
  width: 100%;
  box-sizing: border-box;
`;