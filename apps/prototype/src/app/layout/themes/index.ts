import StyleProvider, { StyleProviderContext } from './style-provider';

export * from './styled.d';

export * from './theme';
export { StyleProviderContext };
export default StyleProvider;
