import { render } from '@testing-library/react';

import SensorTypes from './sensorTypes';

describe('SensorTypes', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<SensorTypes />);
    expect(baseElement).toBeTruthy();
  });
});
